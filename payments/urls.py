from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^invoice/$', views.InvoiceList.as_view(), name='invoice-list'),
    url(r'^invoice/(?P<pk>[0-9]+)/payment/$', views.InvoicePayment.as_view(), name='invoice-payment'),
    url(r'^receipt/$', views.ReceiptList.as_view(), name='receipt-list'),
]
