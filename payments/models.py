from django.db import models
from django.contrib.auth.models import AbstractUser


USER_TYPE_CHOICES = (
    ('C', 'Customer'),
    ('M', 'Merchant')
)


# Create your models here.
class User(AbstractUser):
    user_type = models.CharField(choices=USER_TYPE_CHOICES, max_length=10)


class Invoice(models.Model):
    amount_payable = models.FloatField()
    due_date = models.DateField()
    date_created = models.DateField(auto_now_add=True)
    customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='customer_invoice_set')
    merchant = models.ForeignKey(User, on_delete=models.CASCADE, related_name='merchant_invoice_set')

    def  __str__(self):
        """
        String representation of this model in the format <merchant> to <customer>: <date created>
        """
        return "{} to {}: {}".format(self.merchant.username,
                                     self.customer.username,
                                     self.date_created.strftime('%Y-%m-%d'))


class Receipt(models.Model):
    amount_paid = models.FloatField()
    date_paid = models.DateField(auto_now_add=True)
    date_created = models.DateField(auto_now_add=True)
    customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='customer_receipt_set')
    merchant = models.ForeignKey(User, on_delete=models.CASCADE, related_name='merchant_receipt_set')
    invoice = models.OneToOneField(Invoice)

    def __str__(self):
        """
        String representation of this model in the format <customer> to <merchant>: <date_paid>
        """
        return "{} to {}: {}".format(self.customer.username,
                                     self.merchant.username,
                                     self.date_paid.strftime('%Y-%m-%d'))
