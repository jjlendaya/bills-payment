from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import Invoice, Receipt, USER_TYPE_CHOICES


CUSTOMER_USER_TYPE = USER_TYPE_CHOICES[0][0]


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for the user. For now, this is just a read only serializer
    """
    customer_invoice_set = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    merchant_invoice_set = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    customer_receipt_set = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    merchant_receipt_set = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = get_user_model()
        fields = ('id', 'first_name', 'last_name', 'email', 'password', 'user_type',
                  'customer_invoice_set', 'merchant_invoice_set', 'customer_receipt_set',
                  'merchant_receipt_set')
        read_only_fields = ('id', )
        extra_kwargs = {
            'password': {
                'write_only': True
            }
        }


class InvoiceSerializer(serializers.ModelSerializer):
    """
    Serializer for the invoice
    """
    class Meta:
        model = Invoice
        fields = ('id', 'amount_payable', 'due_date', 'date_created', 'customer', 'merchant')
        read_only_fields = ('id', 'date_created', 'merchant')

    def validate_customer(self, value):
        """
        We validate whether the user receiving the invoice is:
        1) Actually a customer,
        2) Has an email.
        """
        if value.user_type != CUSTOMER_USER_TYPE:
            raise serializers.ValidationError('User is not a customer')
        if value.email is None or value.email == '':
            raise serializers.ValidationError('Customer has no e-mail')
        return value


class ReceiptSerializer(serializers.ModelSerializer):
    """
    Serializer for receipts
    """
    class Meta:
        model = Receipt
        fields = ('id', 'amount_paid', 'date_paid', 'date_created', 'customer', 'merchant', 'invoice')
        read_only_fields = ('id', 'date_paid', 'date_created', 'customer', 'merchant', 'invoice')

    def validate_amount_paid(self, value):
        """
        We validate whether the customer is paying exact amount
        """
        if value != self.context['invoice'].amount_payable:
            raise serializers.ValidationError('Exact payment required')
        return value
