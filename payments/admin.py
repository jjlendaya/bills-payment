from django.contrib import admin
from .models import User, Invoice, Receipt
from django.contrib.auth.admin import UserAdmin


# Register your models here.
class CustomUserAdmin(UserAdmin):
    list_display = UserAdmin.list_display + ('user_type', )
    fieldsets = UserAdmin.fieldsets + ((
        None,
        {
            'fields': ('user_type', )
        }
    ), )
    list_filter = UserAdmin.list_display + ('user_type', )


admin.site.register(User, CustomUserAdmin)
admin.site.register(Invoice)
admin.site.register(Receipt)
