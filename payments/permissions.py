from rest_framework import permissions
from .models import USER_TYPE_CHOICES


CUSTOMER_USER_TYPE = USER_TYPE_CHOICES[0][0]
MERCHANT_USER_TYPE = USER_TYPE_CHOICES[1][0]


class IsCustomer(permissions.BasePermission):
    """
    Permission to check whether a specified user is a customer
    """
    def has_permission(self, request, view):
        return request.user.user_type == CUSTOMER_USER_TYPE


class CanCreateInvoice(permissions.BasePermission):
    """
    Permission to check whether a specified user is a merchant
    and can create an invoice (but only on POST requests)
    """
    def has_permission(self, request, view):
        if request.method == 'POST':
            return request.user.user_type == MERCHANT_USER_TYPE
        return True
