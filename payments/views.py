from django.shortcuts import get_object_or_404
from .serializers import InvoiceSerializer, ReceiptSerializer
from .models import User, Invoice, Receipt, USER_TYPE_CHOICES
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from .permissions import IsCustomer, CanCreateInvoice
from rest_framework.response import Response
from django.utils import timezone
import datetime


CUSTOMER_USER_TYPE = USER_TYPE_CHOICES[0][0]
MERCHANT_USER_TYPE = USER_TYPE_CHOICES[1][0]


# Create your views here.
class InvoiceList(generics.ListCreateAPIView):
    """
    API view for creating and viewing invoices.
    Only Customers can access a pending filter to only see pending invoices.
    """
    serializer_class = InvoiceSerializer
    permission_classes = (IsAuthenticated, CanCreateInvoice)

    def get_queryset(self):
        user = self.request.user
        if user.user_type == CUSTOMER_USER_TYPE:
            pending = self.request.query_params.get('pending', None)
            if pending:
                if pending == 'True':
                    return Invoice.objects.filter(customer=user, receipt=None)
                else:
                    return Invoice.objects.filter(customer=user, receipt__isnull=False)
            else:
                return Invoice.objects.filter(customer=user)
        elif user.user_type == MERCHANT_USER_TYPE:
            return Invoice.objects.filter(merchant=user)

    def perform_create(self, serializer):
        serializer.save(merchant=self.request.user)

    def get_serializer_context(self):
        if self.request.method == 'POST':
            return {
                'customer': get_object_or_404(User, pk=self.request.data['customer'])
            }


class InvoicePayment(generics.CreateAPIView):
    """
    View for creating invoice payments, which is represented by the creation of
    a receipt. Only customers can create payments.
    """
    serializer_class = ReceiptSerializer
    permission_classes = (IsAuthenticated, IsCustomer)

    def get_queryset(self):
        return Invoice.objects.filter(customer=self.request.user)

    def perform_create(self, serializer):
        invoice = get_object_or_404(Invoice, pk=self.kwargs['pk'])
        serializer.save(customer=self.request.user,
                        merchant=invoice.merchant,
                        invoice=invoice,
                        date_paid=timezone.now().date())

    def get_serializer_context(self):
        invoice = get_object_or_404(Invoice, pk=self.kwargs['pk'])
        return {
            'customer': self.request.user,
            'merchant': invoice.merchant,
            'invoice': invoice
        }


class ReceiptList(generics.ListAPIView):
    """
    View for displaying lists of receipts. Only merchants can filter receipts
    by date and week.
    """
    serializer_class = ReceiptSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        user = self.request.user
        if user.user_type == CUSTOMER_USER_TYPE:
            return Receipt.objects.filter(customer=user)
        elif user.user_type == MERCHANT_USER_TYPE:
            date = self.request.query_params.get('date', None)
            week_of = self.request.query_params.get('week_of', None)
            if date:
                return Receipt.objects.filter(merchant=user,
                                              date_paid=datetime.datetime.strptime(date, '%Y-%m-%d').date())
            elif week_of:
                week_of = datetime.datetime.strptime(week_of, '%Y-%m-%d').date()
                week_start = week_of - datetime.timedelta(days=week_of.weekday() + 1)
                week_end = week_start + datetime.timedelta(days=7)
                return Receipt.objects.filter(merchant=user, date_paid__range=[week_start, week_end])
            return Receipt.objects.filter(merchant=user)

    def list(self, request, *args, **kwargs):
        date = self.request.query_params.get('date', None)
        week_of = self.request.query_params.get('week_of', None)

        # Validate request parameters
        if self.request.user.user_type == CUSTOMER_USER_TYPE:
            if date and week_of:
                return Response({
                    'detail': 'Cannot provide both \'date\' and \'week_of\' arguments.'
                }, status=status.HTTP_400_BAD_REQUEST)
            if date:
                try:
                    datetime.datetime.strptime(date, '%Y-%m-%d').date()
                except ValueError:
                    return Response({'detail': 'Invalid argument for \'date\''}, status=status.HTTP_400_BAD_REQUEST)
            if week_of:
                try:
                    datetime.datetime.strptime(week_of, '%Y-%m-%d').date()
                except ValueError:
                    return Response({'detail': 'Invalid argument for \'week\''}, status=status.HTTP_400_BAD_REQUEST)
        return super(ReceiptList, self).list(request, *args, **kwargs)
